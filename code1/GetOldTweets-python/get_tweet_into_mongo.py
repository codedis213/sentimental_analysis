import got, codecs
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['twitter_db']
collection = db['twitter_collection']
tweetCriteria = got.manager.TweetCriteria().setQuerySearch('ChennaiFloods').setSince("2015-12-01").setUntil(
    "2015-12-02").setMaxTweets(
    6000)


def streamTweets(tweets):
    for t in tweets:
        obj = {"user": t.username, "retweets": t.retweets, "favorites":
            t.favorites, "text": t.text, "geo": t.geo, "mentions":
                   t.mentions, "hashtags": t.hashtags, "id": t.id,
               "permalink": t.permalink,}
        tweetind = collection.insert_one(obj).inserted_id


got.manager.TweetManager.getTweets(tweetCriteria, streamTweets)
